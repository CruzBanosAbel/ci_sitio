<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head> <!-- Header starts here -->
	<meta charset="UTF-8" />
	<title>Cafe Nuevo Mundo Oaxaca | Tostadora de café, barra de espresso, panadería y restaurant</title> <!-- Website Title of WordPress Blog -->	
	<link rel="icon" type="image/png"  href="http://cafenuevomundo.com/wp-content/uploads/2012/10/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" href="/style.css" />
	<link rel="pingback" href="http://cafenuevomundo.com/xmlrpc.php" /> <!-- Pingback Call -->
<meta name="description" content="Nuevo Mundo es un tostador de café y barra de espresso ubicado en la ciudad de Oaxaca. Desde el 2002 tostamos granos de Oaxaca, Chiapas y Guerrero. También preparamos los mejores americanos, lattes, cappuccinos y espresso en la ciudad. Contamos con panadería casera. Ven y conócenos!"/>
<!-- / Yoast WordPress SEO plugin. -->
	<style type="text/css">
		.post a, #gallery_prettyphoto.portfolio ul li h3 a, #footer_columns div li a, #footer_copyright a, .post a.read_more, #pagination div a, #comment_form a, #sidebar ul li a, .post .metadata a, .comment a.comment_author, #sidebar a, #sidebar ul#recentcomments li a, p a, .toggle_box a, .accordion a, .accordion div.accordion_head a, .accordion_content a, table a, #pricing_table a, .one_third a, .two_thirds a, one_half a, .message_box a, ul a, ol a, blockquote a, ol.commentlist li.comment div.reply a, .post .metadata_tags a, #pagination-full div.older a {color: #D2D2D2;}
		#gallery_prettyphoto.portfolio ul li h3 a:hover, #footer_columns div li a:hover, #footer_copyright a:hover, .post a.read_more:hover, #pagination div a:hover, #comment_form a:hover, #sidebar ul li a:hover, .post .metadata a:hover, .post a:hover, #sidebar a:hover, #sidebar ul#recentcomments li a:hover, p a:hover, .toggle_box a:hover, .accordion a:hover, .accordion div.accordion_head a:hover, .accordion_content a:hover, table a:hover, #pricing_table a:hover, .one_third a:hover, .two_thirds a:hover, one_half a:hover, .message_box a:hover, ul a:hover, ol a:hover, blockquote a:hover, ol.commentlist li.comment div.reply a:hover, .post .metadata_tags a:hover, #pagination-full div.older a:hover {color: #FF8000;}
		body, #content .post p, #content p, #footer_columns p, #footer_info p, blockquote {color: #CCCCCC;}
		#navbar li a {color: #A3A3A3;}
		#navbar li span {color: #555555;}
		.post h3 a {color: #FFFFFF;}
		.post h3 a:hover {color: ;}
		#navbar li.current-menu-item a, #navbar li.current-menu-ancestor>a, #navbar li.current-menu-parent>a {color: #FFFFFF;} 
		#navbar li.current-menu-item a span, #navbar li.current-menu-ancestor a span, #navbar li.current-post-parent a span, #navbar li.current_page_item .sub-menu li.menu-item a {color: #A3A3A3;}
		#content .post .success p {color: #05CA00;}
		#navbar li ul {background: #000000;}
		#navbar li ul {border: 1px solid #2F2F2F;}
		#gallery_prettyphoto.portfolio ul li {height: px;}
		#content h1 {color: ;}
		#content h2 {color: ;} 
		#content h3, #footer_columns h3, #sidebar h3 {color: ;}
		#content h4 {color: ;} 
		#content h5 {color: ;} 
		#content h6 {color: ;} 
		#main h2.section_title {color: ;} 
		#slidecaption p, #slidecaption h2, #pagination-full div a, #pagination div a, #main h5, #main h4, #main h3, #main h2.section_title, #main h2, #main h1, #navbar li span, h1, h2, h3, h4, h5, #navbar li a {font-family: <!-- Default CSS --> ;}
		#slidecaption h2 {color: ;} 
		#slidecaption p {color: ;}
		#wrapper #slidecaption a {color: ;}
		#wrapper #slidecaption a:hover {color: ;}
	<style>	
		#menu { background: url(http://cafenuevomundo.com/wp-content/themes/kingsize/images/menu_back.png) repeat-y top left !important; }
		#hide_menu { background: url(http://cafenuevomundo.com/wp-content/themes/kingsize/images/hide_menu_back.png) no-repeat bottom left !important; }
		#main_wrap { background: url(http://cafenuevomundo.com/wp-content/themes/kingsize/images/content_back.png) repeat-y top left !important; }
	</style>
</head> 
	<body class="home blog  body_home slider body_portfolio body_colorbox body_gallery_2col_cb slider_details_bottom">
	<div id="wrapper">		
		<!--ACA SE CARGA EL Menu -->
		<?php 
			$this->load->view('sitio/menu');
		?>
		<!-- ACA SE CARGA EL CUERPO -->
		<?php
			$this->load->view($cuerpo);
		?>
		
	</div>	
</body>
</html>