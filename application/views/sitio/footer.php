<div id="footer">		
	<div id="footer_info"><!-- Footer columns -->
		<div id="footer_columns">						
			<div>
				<div id="text-6" class="widget-container widget_text">			
					<div class="textwidget">
						<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FCafeOaxaca&amp;width=250&amp;height=258&amp;colorscheme=dark&amp;show_faces=true&amp;border_color=%23000&amp;stream=false&amp;header=false&amp;appId=135564713201545" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:250px; height:258px;" allowTransparency="true">
						</iframe>
					</div>
				</div>								
			</div>
			<div>
				<div id="kingsize_contactinfo_widget-2" class="widget-container widget_kingsize_contactinfo">
					<h3>Café Nuevo Mundo</h3>
					<div class="sidebar_item">
						<ul class="contact_list">
							<li class="contact_phone">(52) 951 -50- 121- 22</li>
							<li class="contact_email">nmroasters@gmail.com</li>
							<li class="contact_address">M. Bravo 206, Oaxaca, México</li>
						</ul>
					</div>
				</div>								
			</div>							
			<div class="last">
				<div id="text-5" class="widget-container widget_text">
					<h3 class="widget-title">Reviews:</h3>			
					<div class="textwidget">
						"They care about coffee here. Great roast, great blend. Take some home. I did." Michael
					</br>
					</br>
					</br>
					"One of the best places in the world for coffee! I had coffee at "La Taza dr Oro" in Rome which claims to serve the best coffee in the world and "Nuevo Mundo" is equivalent. Had the breakfast package wih orange juice, capuccino, fried eggs with cactus, toast and an oatmeal cookie for about 5 dollars, a bargain and a treat. They roast coffee on the premises and you can buy it there. The place is a bit spartan in decor, crowded, but worth a visit to please your taste buds. Enjoy!" Alffiemexicocity</div>
				</div>	
			</div>								
		</div>										
		<div id="footer_copyright">
			<p class="copyright"><!-- Hcard -->
				<div id="hcard-Cafe-Nuevo-Mundo" class="vcard">
	 				<a class="url fn n" href="http://www.cafenuevomundo.com">  
	 					<span class="given-name">Café Nuevo Mundo</span>
	  					<span class="additional-name"></span>
	  					<span class="family-name"></span>
					</a>
	 				<a class="email" href="mailto:juan@cafenuevomundo.com">juan@cafenuevomundo.com</a>
	 				<div class="adr">
	  					<div class="street-address">
	  						M. Bravo 206
		  				</div>
		  				<span class="locality">Oaxaca</span>
							, 
		  				<span class="region">Oaxaca</span>
							, 
		  				<span class="postal-code">68000</span>	
		  				<span class="country-name">México</span>
	 				</div>
	 				<div class="tel">01 951 501 2122
	 				</div><!-- Hcard -->
	 			</div>
		 	</p>
			<ul class="social">									
			<!-- SOCIAL ICONS -->
					<li><a href="cafeoaxaca"><img src="http://cafenuevomundo.com/wp-content/themes/kingsize/images/social/facebook_16.png" alt="facebook" title="Facebook" class="tooltip_link"></a></li>
					<!-- SOCIAL ICONS -->						
			</ul>
		</div><!-- Copyright / Social Footer Ends Here -->		
	</div>						
</div><!-- Footer ends here --> 