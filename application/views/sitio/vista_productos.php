<!-- Main wrap -->
<div id="main_wrap">			
<!-- Main -->
	<div id="main">   			 	
		<h2 class="section_title">Productos</h2><!-- This is your section title --> 
		<div id="content" class="content_full_width">
			<p>Nuevo Mundo es distribuidor en Oaxaca de los siguientes productos:</p>
			<p><strong>Da Vinci Gourmet </strong>– Jarabes (Siropes) especiales para saborizar bebidas calientes y frías.</p>
			<p>Sabores clásicos:<br />
			Amaretto, avellana, crema irlandesa, B- 52, azul de curacao, kiwi, limón, fresa, mango, coco, piña, etc. (preguntar por más sabores)</p>
			<p>Fruit Innovation:<br />
			Fresa, kiwi, honey-melon, tropical, asian fruits, arándano, manzana verde, etc (preguntar por más sabores)</p>
			<p>Salsas gourmet:<br />
			Chocolate oscuro, chocolate blanco, caramel</p>
			<p>Tés:<br />
			Chai clásico y sugar free, green tea</p>
			<p><strong>Oregon Chai</strong> – línea de concentrados en polvo y líquidos de Té Chai para preparar bebidas étnicas e indulgentes<br />
			Original, sugar free, manzana canela, vainilla, té verde.</p>
			<p><strong>Frappease</strong> – Base láctea en polvo para preparar deliciosos frappés<br />
			Original, Crystal, yogurt, sugar free.</p>
			<p><strong>Maruka</strong> – limón en polvo listo para hidratar, ideal para preparar bebidas y cócteles.</p>
			<p><strong>Oscar&#8217;s –</strong> Línea de jarabes (Siropes) para saborizar café.</p>
			<p><strong>Caffé D’ amore</strong><br />
			Moka freeze, Latte freeze, coffee toffee, cookies n cream freeze, east indian spice chai, matcha green tea, Bellagio sipping chocolate.</p>
			<p><strong><a href="http://cafenuevomundo.com/contacto/">Contáctanos para precios e información!</a></strong></p>
			<p>&nbsp;</p>
			<p><strong><img class="aligncenter size-full wp-image-1198" title="saborizantes_cafe_oaxaca" src="http://cafenuevomundo.com/wp-content/uploads/2012/10/saborizantes_cafe_oaxaca.jpg" alt="" width="700" height="467" /><br />
			</strong></p>
			<iframe class="fblikes" src="http://www.facebook.com/plugins/like.php?href=http://cafenuevomundo.com/productos/&amp;send=false&amp;layout=standard&amp;width=600&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:600px; height:80px; margin: 0px 0px 0px 0px;" allowTransparency="true"></iframe>				
		</div>
	</div>
	<?php
		$this->load->view("sitio/footer");
	?>
</div>