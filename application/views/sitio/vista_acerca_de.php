<!-- Main wrap -->
<div id="main_wrap">
<!-- Main -->
	<div id="main">   			 	
		<h2 class="section_title">Acerca de</h2><!-- This is your section title -->  		
	    <div id="content" class="content_full_width">			
			<p style="text-align: center;"><img class="aligncenter size-full wp-image-1196" title="cafe_oaxaca_nuevo_mundo" src="http://cafenuevomundo.com/wp-content/uploads/2009/11/cafe_oaxaca_nuevo_mundo.jpg" alt="El mejor cafe de Oaxaca" width="700" height="467" /></p>
			<p style="text-align: left;">Nuevo Mundo Coffee Roaster es una micro tostadora de café independiente fundada en enero del 2002, con la finalidad de incrementar la cultura del café en la ciudad de Oaxaca al tiempo que impulsaba el consumo del aromático mexicano.<br />
			Trabajamos con pequeños productores de café arábiga en los estados de Oaxaca, Chiapas y Guerrero, los cuales nos proporcionan café orgánico verde que nosotros tostamos y mezclamos para ofrecer un café fresco y buen sabor.</p>
			<p style="text-align: left;">Entre las mezclas que tenemos están:</p>
			<p style="text-align: left;"><strong>Mezcla especial de la casa</strong><br />
			Suave, aromática y un tanto afrutada. Esta mezcla recomendamos sea preparada con cafeteras de filtro de papel, Prensa Francesa o en la Olla.</p>
			<p style="text-align: left;"><strong>Mezcla de Espresso Clásico</strong>:<br />
			Esta mezcla contiene un secreto especial que le otorga mayor cafeína a cada taza. Es recomendada para aquellos que cuentan con una maquina de espresso, aunque también puede prepararse usando la cafetera italiana.<br />
			Las mezclas estan disponibles en tostado medio y tostado oscuro.</p>
			<p style="text-align: left;"><strong>Cafe Descafeinado</strong>:<br />
			Cafe organico de Oaxaca que se descafeina a traves del metodo de agua, el mas seguro de todos.<br />
			Para amantes del cafe pero que por alguna razon no toleran la cafeina.</p>
			<p style="text-align: left;">Nuevo Mundo también es reconocido por su barra de bebidas a base de espresso, sin olvidar claro los deliciosos Smoothies de mango y fresa que expendemos. Tambien contamos con asesoria a restaurantes, cafeterias y hoteles que consumen nuestro cafe para lograr mejores resultados en la preparacion de bebidas a base de espresso.</p>
			<p style="text-align: left;">Visítanos y no te arrepentirás!.</p>
			<p style="text-align: center;">
			<div id="TA_excellent88" class="TA_excellent">
				<ul id="xsIrT9qJWf" class="TA_links 9NCYAO">
					<li id="Qg4QLvqXOkT" class="mJzXeZD1e7V"><a target="_blank" href=http://www.tripadvisor.com/Restaurant_Review-g150801-d1575242-Reviews-Nuevo_Mundo_Coffee_Roaster-Oaxaca_Southern_Mexico.html>Nuevo Mundo Coffee Roaster</a> rated &#8220;excellent&#8221; by travelers</li>
				</ul>
			</div>
			<p><script src="http://www.jscache.com/wejs?wtype=excellent&amp;uniq=88&amp;locationId=1575242&amp;lang=es_MX"></script></p>
			<iframe class="fblikes" src="http://www.facebook.com/plugins/like.php?href=http://cafenuevomundo.com/about/&amp;send=false&amp;layout=standard&amp;width=600&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:600px; height:80px; margin: 0px 0px 0px 0px;" allowTransparency="true"></iframe>																				
			<div id="comments">
				<p class="nocomments"></p>
			</div><!-- #comments -->
	    </div> 
	</div>		
	<?php
		$this->load->view("sitio/footer");
	?>	
</div>
