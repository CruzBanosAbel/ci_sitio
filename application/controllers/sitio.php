<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitio extends CI_Controller {
	private $datos;
	public function __construct(){
		parent::__construct();
		$this->datos = array(
			'cuerpo' => '',
		);
	}
	public function index()
	{
		$this->datos['cuerpo'] = 'sitio/vista_bienvenida';
  		$this->load->view('sitio/vista_base.php', $this->datos);
	}
	public function about(){
		$this->datos['cuerpo'] = 'sitio/vista_about';
		$this->load->view('sitio/vista_base.php', $this->datos);
	}
	public function acercade(){
		$this->datos['cuerpo'] = 'sitio/vista_acerca_de';
		$this->load->view('sitio/vista_base.php', $this->datos);
	}
	public function productos(){
		$this->datos['cuerpo'] = 'sitio/vista_productos';
		$this->load->view('sitio/vista_base.php', $this->datos);
	}
	public function mapa(){
		$this->datos['cuerpo'] = 'sitio/vista_mapa';
		$this->load->view('sitio/vista_base.php', $this->datos);
	}
	public function contacto(){
		$this->datos['cuerpo'] = 'sitio/vista_contacto';
		$this->load->view('sitio/vista_base.php', $this->datos);
	}
}
